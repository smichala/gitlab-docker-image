FROM python:3.8-slim-buster

RUN pip --no-cache-dir install aws-sam-cli awscli pytest pytest-mock

#testen:
#- pip3 install pytest
#- pip3 install pytest-mock --user
#- pip3 install awscli --upgrade
#- pip3 install aws-sam-cli --upgrade

RUN adduser samcli --disabled-password && echo 'samcli ALL=(ALL) NOPASSWD:ALL' >>/etc/sudoers

USER samcli

WORKDIR /home/samcli